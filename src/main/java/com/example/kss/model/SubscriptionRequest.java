package com.example.kss.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SubscriptionRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubscriptionRequest {

	@XmlElement(name = "Activate")
	private Activate activate;

	@XmlElement(name = "HardCancel")
	private HardCancel hardCancel;

	@XmlElement(name = "Renew")
	private Renew renew;

	@XmlElement(name = "GetInfo")
	private GetInfo info;

	public SubscriptionRequest() {
		super();
	}

	public SubscriptionRequest(Activate activate) {
		super();
		this.activate = activate;
	}

	public SubscriptionRequest(HardCancel hardCancel) {
		super();
		this.hardCancel = hardCancel;
	}

	public SubscriptionRequest(Renew renew) {
		super();
		this.renew = renew;
	}

	public SubscriptionRequest(GetInfo info) {
		super();
		this.info = info;
	}

	public SubscriptionRequest(Activate activate, HardCancel hardCancel, Renew renew, GetInfo info) {
		super();
		this.activate = activate;
		this.hardCancel = hardCancel;
		this.renew = renew;
		this.info = info;
	}

	public Activate getActivate() {
		return activate;
	}

	public void setActivate(Activate activate) {
		this.activate = activate;
	}

	public HardCancel getHardCancel() {
		return hardCancel;
	}

	public void setHardCancel(HardCancel hardCancel) {
		this.hardCancel = hardCancel;
	}

	public Renew getRenew() {
		return renew;
	}

	public void setRenew(Renew renew) {
		this.renew = renew;
	}

	public GetInfo getInfo() {
		return info;
	}

	public void setInfo(GetInfo info) {
		this.info = info;
	}

}
