package com.example.kss.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import com.example.kss.config.KssSystemConfig;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpUtil {
	
	private static Logger log = LogManager.getLogger(HttpUtil.class);
	
	/**
	 * 讀取金鑰 Load the .JKS from files.
	 * @param path
	 * @param fileName
	 * @param type
	 * @param pwd
	 * @return
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public static KeyStore loadKeyStore(String path, String fileName,  String type, String pwd) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException  {
		InputStream keyStoreStream = new FileInputStream(path + "/" + fileName);
		KeyStore keyStore = KeyStore.getInstance(type); //"JKS" or "PKCS12"
		keyStore.load(keyStoreStream, pwd.toCharArray());
		return keyStore;
	}
	
	/**
	 * @param in
	 * @param encode
	 * @return
	 * @throws IOException
	 */
	public static String getStringFromStream(InputStream in, String encode) throws IOException {
//		log.debug("encode{}", encode);
		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = null;
		String rtnResponse = "";
		reader = new BufferedReader(new InputStreamReader(in, encode));
		String line = null;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		rtnResponse = buffer.toString();
		reader.close();
		return rtnResponse;
	}
	
	public static ConnectionKeepAliveStrategy getKeepAliveStrategy() throws IOException {
		ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
		    @Override
		    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
		        HeaderElementIterator it = new BasicHeaderElementIterator
		            (response.headerIterator(HTTP.CONN_KEEP_ALIVE));
		        while (it.hasNext()) {
		            HeaderElement he = it.nextElement();
		            String param = he.getName();
		            String value = he.getValue();
		            if (value != null && param.equalsIgnoreCase
		               ("timeout")) {
		                return Long.parseLong(value) * 1000;
		            }
		        }
		        return 60 * 1000;
		    }
		};
		 return myStrategy;
	}
	
	/**
	 * send request with client certificate only for kss
	 * @param url
	 * @param reqBody
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws KeyManagementException
	 * @throws UnrecoverableKeyException
	 */
	public static String doPost(String url, String reqBody) throws ClientProtocolException, IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException, KeyManagementException, UnrecoverableKeyException {

		log.error("thread id={}, request url={}",Thread.currentThread().getId(), url);
		log.error("thread id={}, request Body={}",Thread.currentThread().getId(), reqBody);
		
		KeyStore  keyStore = loadKeyStore(KssSystemConfig.PATH, KssSystemConfig.CERT_FILE_NAME, "JKS",  KssSystemConfig.ACCESS_INFO_PWD);
		
		SSLContext sslContext = SSLContexts.custom().loadKeyMaterial(keyStore, KssSystemConfig.ACCESS_INFO_PWD.toCharArray())
													.build();
		HttpClient httpClient = HttpClients.custom().setKeepAliveStrategy(getKeepAliveStrategy())
													.setSSLContext(sslContext)
													.build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Content-Type", "application/xml");
		httpPost.setEntity(new StringEntity(reqBody, StandardCharsets.UTF_8));
		
		HttpResponse response = httpClient.execute(httpPost);

		String responseBody = getStringFromStream(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
		log.error("thread id={}, response Body={}",Thread.currentThread().getId(), responseBody);
		return responseBody;
	}
	
	
	
	
}
