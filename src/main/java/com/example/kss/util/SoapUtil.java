package com.example.kss.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import javax.xml.soap.*;

import com.example.kss.model.AccessInfo;
import com.example.kss.model.SubscriptionRequestContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class SoapUtil {
	
	private static Logger log = LogManager.getLogger(SoapUtil.class);
	private static SoapUtil instance = null;
	
	public static SoapUtil getInstance() {
        if(instance == null) {
            synchronized (SoapUtil.class) {
                if (instance == null) {
                    instance = new SoapUtil();
                }
            }
        }
        return instance;
    }
	
	/**
	 *  only for kss use
	 * @param accessInfo
	 * @param container
	 * @return
	 * @throws SOAPException 
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	public String generateSoapMsgStr(AccessInfo accessInfo, SubscriptionRequestContainer container) throws SOAPException, JAXBException, IOException {
		
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		SOAPHeader header = soapEnvelope.getHeader();
		SOAPBody soapBody = soapEnvelope.getBody();

		JAXBContext jaxbContext = JAXBContext.newInstance(accessInfo.getClass(), container.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//          jaxbMarshaller.setProperty(CharacterEscapeHandler.class.getName(), new CustomCharacterEscapeHandler());
//         jaxbMarshaller.setProperty("com.sun.xml.internal.bind.characterEscapeHandler",
//                 new CharacterEscapeHandler() {
//                     public void escape(char[] ch, int start, int length, boolean isAttVal, Writer writer)
//                             throws IOException {
//                         writer.write(ch, start, length);
//                     }
//                 });

		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.name());

        jaxbMarshaller.marshal(accessInfo, header);
		jaxbMarshaller.marshal(container, soapBody);
		soapMessage.saveChanges();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		soapMessage.writeTo(os);
		String soapMsgStr = new String(os.toByteArray(), StandardCharsets.UTF_8);
		os.close();
		return soapMsgStr;
	}
	
	/**
     * 把soap字符串轉爲SOAPMessage
     *
     * @param soapString
     * @return
	 * @throws SOAPException 
	 * @throws IOException 
     */
    public SOAPMessage strXMLToSoapMsg(String soapString) throws SOAPException, IOException {
    	MessageFactory msgFactory = MessageFactory.newInstance();
        SOAPMessage reqMsg = msgFactory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soapString.getBytes(StandardCharsets.UTF_8)));
        reqMsg.saveChanges();
        return reqMsg;
    }
    
    /**
     * xml 資料轉 json格式
     * @param iterator
     * @param nodeName
     * @return
     * @throws JSONException 
     */
	public String xmlToJson(Iterator<SOAPElement> iterator, String nodeName) throws JSONException {
		String responseBody = "";
		JSONObject jsonObj = new JSONObject();
		//有以下tag name 才將資料轉出
		List<String> commandList = Arrays.asList(	"Activate", "Renew", "HardCancel", "SoftCancel", "Pause", 
													"Resume", "GetDownloadLinks", "Subscription", "SubscriptionUsage",
													"ActivateError", "RenewError", "HardCancelError", "SoftCancelError", "PauseError", 
													"ResumeError", "GetDownloadLinksError", "GetInfoError",
													"TransactionError");
		while (iterator.hasNext()) {
			Object obj = iterator.next();
			if (!Objects.isNull(obj) && obj instanceof SOAPElement) {
				SOAPElement element = (SOAPElement) obj;
//				log.error("Node Name:" + element.getNodeName());
				if (commandList.contains(element.getNodeName())) { 
					for (Iterator itr = element.getAllAttributes(); itr.hasNext();) {
						Name attributeName = (Name) itr.next();
						jsonObj.put(attributeName.getLocalName(), element.getAttributeValue(attributeName));
					}
					log.error("thread id={}, response json={}", Thread.currentThread().getId(), jsonObj.toString());
					responseBody = jsonObj.toString();
					return responseBody;
				} 
				
				//XXX recursion
				if (!Objects.isNull(element)) {
					responseBody = xmlToJson(element.getChildElements(), nodeName);
				}
			}
		}
		return responseBody;
	}
}
